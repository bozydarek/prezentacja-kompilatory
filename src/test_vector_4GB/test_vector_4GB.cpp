#include <iostream>
#include <vector>

using namespace std;

int main(){
    
    vector<int> test;

    for(long long int i=0; i<1000000000; ++i){

        if( i % 3 == 0)
        {
            test.push_back(3*i);
        }
        else if( i % 31 == 0)
        {
            test.push_back(31*i);
        }
        else
        {
            test.push_back(42*i);
        }
    }

    int a = test[1] + test[10] + test[100];
    int b = a * a - a;
    int c = b % 10;    

    return c;
}
