#!/bin/bash

if [[ $# -lt 1 ]] || [[ $# -gt 2 ]]; then
    echo $'\033[1;31mZła ilość argumentów \033[0m'
    echo $'\033[1mPrzykład użycia:\033[0m'
    echo $'tester program [ilosc_testow]'
    exit 1
fi

tests=10

if [[ $# -eq 2 ]]; then
    tests=$2
fi

sys_all=0.0
usr_all=0.00
mem_all=0

for (( i=0; i<$tests; i++ )) do
    out=$( { /usr/bin/time -f '%S %U %M' $1; } 2>&1 );

#    echo "out: #" $out "#";

    sys=$( echo $out | awk 'END {print $(NF-2)}' );
    usr=$( echo $out | awk 'END {print $(NF-1)}' );
    mem=$( echo $out | awk 'END {print $NF}' );
    
#    echo "SYS:" $sys "#"
#    echo "MEM:" $mem "#"
        
    sys_all=$(echo "$sys_all + $sys" | bc -l)
    usr_all=$(bc -l <<< "$usr_all + $usr")
    mem_all=$(bc <<< "$mem_all + $mem")
    
done

sys_ans=$(bc -l <<< "$sys_all/$tests")
usr_ans=$(bc -l <<< "$usr_all/$tests")
mem_ans=$(bc <<< "$mem_all/$tests")

echo $(bc -l <<< "$sys_ans+$usr_ans") "sec"
echo $mem_ans "kbytes"
