#!/bin/bash

name="unordered_set.cpp"
binName="unordered_set"

options="-std=c++14 -O3 -o"

n=5

g++-5 $name $options "$binName"GCC5

g++-7 $name $options "$binName"GCC7


clang++-3.8 $name $options "$binName"CLANG3.8

clang++-5.0 $name $options "$binName"CLANG5.0

icc $name $options "$binName"ICC


echo "GCC 5.4"
../tester.sh ./"$binName"GCC5 $n

echo "GCC 7.2"
../tester.sh ./"$binName"GCC7 $n

echo "CLANG 3.8"
../tester.sh ./"$binName"CLANG3.8 $n

echo "CLANG 5.0"
../tester.sh ./"$binName"CLANG5.0 $n

echo "ICC 18"
../tester.sh ./"$binName"ICC $n
