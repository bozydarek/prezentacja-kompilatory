#include <unordered_set>
#include <fstream>
#include <string>

using namespace std;

int main()
{
    ifstream stream;

    std::string word;

    unordered_set<string> set;

    stream.open("words.txt", std::ifstream::in);


    while(getline(stream, word))
    {
        set.insert(word);
    }

    stream.close();

    return 0;
}
